// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import VueMaterial from 'vue-material';

Vue.config.productionTip = false;

global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;

Vue.use(VueMaterial);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  data: {
    purchases: [],
    purchase: null,
    user: {
      org: 'KartofanLTD'
    }
  }
});

$('.md-select').on('click', function () {
  $(this).toggleClass('active');
});

$('.md-select ul li').on('click', function () {
  var v = $(this).text();
  $('.md-select ul li').not($(this)).removeClass('active');
  $(this).addClass('active');
  $('.md-select label button').text(v);
});
