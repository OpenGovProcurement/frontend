import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Gov from '@/components/Gov';
import Org from '@/components/Org';
import OrgAdd from '@/components/OrgAdd';
import GovAdd from '@/components/GovAdd';

Vue.use(Router);

const API_ENDPOINT = '//localhost:8000';

const store = {

  purchases: [],
  user: { org: 'Kartofan, Inc.', name: 'Ivan' },

  updatePurchases() {
    return fetch(API_ENDPOINT + '/api/v1/purchase?limit=8').then(res => res.json()).then(data => {
      this.purchases = data;
    }).catch(err => {
      console.error('failed to fetch purchases data:', err);
    });
  },

  updateUser() {
    return fetch(API_ENDPOINT + '/api/v1/user').then(res => res.json()).then(data => {
      this.user = data;
    }).catch(err => {
      console.error('failed to fetch user information:', err);
    });
  },

  updatePurchaseInfo(purchaseId) {
    fetch(API_ENDPOINT + '/api/v1/purchase/' + purchaseId).then(res => res.json()).then(data => {
      this.purchase = data;
    }).catch(err => {
      console.error('failed to fetch purchase data:', err);
    });
  }
};

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      props: {
        default: true,
        store
      }
    },
    {
      path: '/gov',
      name: 'Government Purchases',
      component: Gov,
      props: {
        default: true,
        store
      }
    },
    {
      path: '/gov/add',
      name: 'Add purchase',
      component: GovAdd,
      props: {
        default: true,
        store
      }
    },
    {
      path: '/org/',
      name: 'Organization',
      component: Org,
      props: {
        default: true,
        store
      }
    },
    {
      path: '/org/add/:purchaseId',
      name: 'Add offer',
      component: OrgAdd,
      props: {
        default: true,
        store
      }
    }
  ]
});
